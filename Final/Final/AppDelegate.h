//
//  AppDelegate.h
//  Final
//
//  Created by ICTC User on 11/12/16.
//  Copyright (c) 2016 ICTC User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

