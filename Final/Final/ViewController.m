//
//  ViewController.m
//  Final
//
//  Created by ICTC User on 11/12/16.
//  Copyright (c) 2016 ICTC User. All rights reserved.
//

#import "ViewController.h"
#import "DSLCalendarView.h"
@interface ViewController ()<DSLCalendarViewDelegate>

@property (nonatomic, weak) IBOutlet DSLCalendarView *calendarView;

@end

@implementation ViewController

- (void)showUIAlertWithMessage: (NSString *) message andTitle: (NSString *) title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.calendarView.delegate = self;
    
    NSString *docsDir;
    NSArray *dirPath;
    
    //Get Directory
    dirPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPath[0];
    
    //Build path to keep database
    _databasePath = [docsDir stringByAppendingPathComponent:@"final.db"];
    
    NSLog(@"%@",_databasePath);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:_databasePath]== NO) {
        const char *dbPath = [_databasePath UTF8String];
        if(sqlite3_open(dbPath, &_DB)== SQLITE_OK)
        {
            char *errorMessage;
            const char *sql_statement = "CREATE TABLE IF NOT EXISTS events (ID INTEGER PRIMARY KEY AUTOINCREMENT, TITLE TEXT, DESCRIPTION TEXT, DATEFROM DATE, DATETO DATE); CREATE TABLE IF NOT EXISTS tasks (ID INTEGER PRIMARY KEY AUTOINCREMENT, TITLE TEXT, DESCRIPTION TEXT)";
            if(sqlite3_exec(_DB, sql_statement, NULL, NULL, &errorMessage) != SQLITE_OK)
            {
                NSLog(@"%s",errorMessage);
                [self showUIAlertWithMessage:@"Failed to create table" andTitle:@"Error"];
            }
        }
    }
    else
    {
        NSLog(@"[INIT]Open Failure: %s",sqlite3_errmsg(_DB));
        [self showUIAlertWithMessage:@"Failed to open/create table" andTitle:@"Error"];
    }
    
    sqlite3_close(_DB);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

//- (IBAction)addEvent:(id)sender {
//    sqlite3_stmt *statement;
//    const char *dbPath = [_databasePath UTF8String];

    //open Database
//    if(sqlite3_open(dbPath, &_DB)==SQLITE_OK)
//    {
//        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO users (NAME, ADDRESS, PHONE) VALUES (\"%@\",\"%@\",\"%@\")",name.text, address.text,phone.text];
//        const char *insertStatement = [insertSQL UTF8String];
//        
//        //preparing sql statement for execution
//        if(sqlite3_prepare_v2(_DB, insertStatement, -1, &statement, NULL)!=SQLITE_OK)
//        {
//            NSLog(@"[ADD EVENT]Prepare Failure: %s",sqlite3_errmsg(_DB));
//        }
//        
//        //re-run the sql statement for any schema modification
//        if (sqlite3_step(statement)!=SQLITE_DONE) {
//            NSLog(@"[ADD EVENT]Step Failure: %s", sqlite3_errmsg(_DB));
//            [self showUIAlertWithMessage:@"FAILED to add user" andTitle:@"Error"];
//        }
//        
//        //sql statement is successful
//        else
//        {
//            [self showUIAlertWithMessage:@"User added" andTitle:@"Message"];
//            name.text = @"";
//            address.text = @"";
//            phone.text = @"";
//        }
//    }
//    else
//    {
//        NSLog(@"[INIT]Open Failure: %s",sqlite3_errmsg(_DB));
//        [self showUIAlertWithMessage:@"Failed to open DBASE" andTitle:@"Error"];
//    }
//    sqlite3_finalize(statement);
//    sqlite3_close(_DB);
//    
//    
//}
//
//- (IBAction)findEvent:(id)sender {
//    sqlite3_stmt *statement;
//    const char *dbPath = [_databasePath UTF8String];
//    
    //open Database
//    if(sqlite3_open(dbPath, &_DB)==SQLITE_OK)
//    {
//        NSString *insertSQL = [NSString stringWithFormat:@"SELECT * FROM users WHERE NAME ==  \"%@\"",name.text];
//        const char *insertStatement = [insertSQL UTF8String];
//        
//        //preparing sql statement for execution
//        if(sqlite3_prepare_v2(_DB, insertStatement, -1, &statement, NULL)!=SQLITE_OK)
//        {
//            NSLog(@"[FIND EVENT]Prepare Failure: %s",sqlite3_errmsg(_DB));
//            
//        }
//        else
//        {
//            
//        }
//        //re-run the sql statement for any schema modification
//        if (sqlite3_step(statement)!=SQLITE_DONE) {
//            NSLog(@"[FIND EVENT]Step Failure: %s", sqlite3_errmsg(_DB));
//            [self showUIAlertWithMessage:@"FAILED to find user" andTitle:@"Error"];
//        }
//        
//        //sql statement is successful
//        else
//        {
//            [self showUIAlertWithMessage:@"User found" andTitle:@"Message"];
//            
//            
//            address.text = @"";
//            phone.text = @"";
//        }
//    }
//    else
//    {
//        NSLog(@"[INIT]Open Failure: %s",sqlite3_errmsg(_DB));
//        [self showUIAlertWithMessage:@"Failed to open DBASE" andTitle:@"Error"];
//    }
//    sqlite3_finalize(statement);
//    sqlite3_close(_DB);
    
//}

//- (IBAction)deleteEvent:(id)sender {
//    sqlite3_stmt *statement;
//    const char *dbPath = [_databasePath UTF8String];
    
    //open Database
//    if(sqlite3_open(dbPath, &_DB)==SQLITE_OK)
//    {
//        NSString *insertSQL = [NSString stringWithFormat:@"DELETE FROM USERS WHERE NAME == \"%@\" ",name.text];
//        const char *insertStatement = [insertSQL UTF8String];
//        
//        //preparing sql statement for execution
//        if(sqlite3_prepare_v2(_DB, insertStatement, -1, &statement, NULL)!=SQLITE_OK)
//        {
//            NSLog(@"[DELETE EVENT]Prepare Failure: %s",sqlite3_errmsg(_DB));
//        }
//        
//        //re-run the sql statement for any schema modification
//        if (sqlite3_step(statement)!=SQLITE_DONE) {
//            NSLog(@"[DELETE EVENT]Step Failure: %s", sqlite3_errmsg(_DB));
//            [self showUIAlertWithMessage:@"FAILED to delete user" andTitle:@"Error"];
//        }
//        
//        //sql statement is successful
//        else
//        {
//            NSLog(@"%i",sqlite3_total_changes(_DB));
//            if(sqlite3_total_changes(_DB)==0)
//            {
//                [self showUIAlertWithMessage:@"FAILED to delete" andTitle:@"Message"];
//            }
//            else
//            {
//                [self showUIAlertWithMessage:@"User deleted" andTitle:@"Message"];
//                name.text = @"";
//            }
//        }
//    }
//    else
//    {
//        NSLog(@"[INIT]Open Failure: %s",sqlite3_errmsg(_DB));
//        [self showUIAlertWithMessage:@"Failed to open DBASE" andTitle:@"Error"];
//    }
//    sqlite3_finalize(statement);
//    sqlite3_close(_DB);
//    
//    
//}

@end
