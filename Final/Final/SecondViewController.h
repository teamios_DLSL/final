//
//  SecondViewController.h
//  Final
//
//  Created by ICTC User on 11/19/16.
//  Copyright (c) 2016 ICTC User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *inputEventTitle;
@property (weak, nonatomic) IBOutlet UITextView *inputEventDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UIDatePicker *eventDateFrom;
@property (weak, nonatomic) IBOutlet UIDatePicker *eventDateEnd;

@property (weak, nonatomic) IBOutlet UITextField *inputTaskTitle;
@property (weak, nonatomic) IBOutlet UITextView *inputTaskDescription;
@property (weak, nonatomic) IBOutlet UIDatePicker *taskDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UISegmentedControl *privateSgmntdCtrlEventTask;

- (IBAction)selectTaskOrEvent:(id)sender;
- (IBAction)buttonDone:(id)sender;

@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *DB;

@end
