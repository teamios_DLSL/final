//
//  ViewController.h
//  Final
//
//  Created by ICTC User on 11/12/16.
//  Copyright (c) 2016 ICTC User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *DB;

@end

