//
//  main.m
//  Final
//
//  Created by ICTC User on 11/12/16.
//  Copyright (c) 2016 ICTC User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
