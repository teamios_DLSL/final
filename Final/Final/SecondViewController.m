//
//  SecondViewController.m
//  Final
//
//  Created by ICTC User on 11/19/16.
//  Copyright (c) 2016 ICTC User. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

@synthesize privateSgmntdCtrlEventTask, lblDate, lblFrom, lblTo, inputEventDescription, inputEventTitle, inputTaskDescription, inputTaskTitle, eventDateEnd, eventDateFrom, taskDate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self hideTaskLayout];
    
//    NSString *docsDir;
//    NSArray *dirPath;
//    
//    //Get Directory
//    dirPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    docsDir = dirPath[0];
//    
//    //Build path to keep database
//    _databasePath = [docsDir stringByAppendingPathComponent:@"final.db"];
//    
//    NSLog(@"%@",_databasePath);
//    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    if ([fileManager fileExistsAtPath:_databasePath]== NO) {
//        const char *dbPath = [_databasePath UTF8String];
//        if(sqlite3_open(dbPath, &_DB)== SQLITE_OK)
//        {
//            char *errorMessage;
//            const char *sql_statement = "CREATE TABLE IF NOT EXISTS events (ID INTEGER PRIMARY KEY AUTOINCREMENT, TITLE TEXT, DESCRIPTION TEXT, DATEFROM DATE, DATETO DATE); CREATE TABLE IF NOT EXISTS tasks (ID INTEGER PRIMARY KEY AUTOINCREMENT, TITLE TEXT, DESCRIPTION TEXT)";
//            if(sqlite3_exec(_DB, sql_statement, NULL, NULL, &errorMessage) != SQLITE_OK)
//            {
//                NSLog(@"%s",errorMessage);
//                [self showUIAlertWithMessage:@"Failed to create table" andTitle:@"Error"];
//            }
//        }
//    }
//    else
//    {
//        NSLog(@"[INIT]Open Failure: %s",sqlite3_errmsg(_DB));
//        [self showUIAlertWithMessage:@"Failed to open/create table" andTitle:@"Error"];
//    }
//    
//    sqlite3_close(_DB);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)selectTaskOrEvent:(id)sender {
    switch (privateSgmntdCtrlEventTask.selectedSegmentIndex) {
        case 0:
            lblDate.hidden = true;
            inputTaskTitle.hidden = true;
            inputTaskDescription.hidden = true;
            taskDate.hidden = true;
            
            inputEventTitle.hidden = false;
            inputEventDescription.hidden = false;
            lblFrom.hidden = false;
            lblTo.hidden = false;
            eventDateFrom.hidden = false;
            eventDateEnd.hidden = false;
            break;
        case 1:
            lblFrom.hidden = true;
            lblTo.hidden = true;
            inputEventDescription.hidden = true;
            inputEventTitle.hidden = true;
            eventDateFrom.hidden = true;
            eventDateEnd.hidden = true;
            
            lblDate.hidden = false;
            inputTaskTitle.hidden = false;
            inputTaskDescription.hidden = false;
            taskDate.hidden = false;
            break;
        default:
            break;
    }
}



- (IBAction)buttonDone:(id)sender {
    NSLog(@"Done");
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
}

- (void)hideTaskLayout {
    lblDate.hidden = true;
    inputTaskTitle.hidden  = true;
    inputTaskDescription.hidden = true;
    taskDate.hidden = true;
    
    inputEventTitle.hidden = false;
    inputEventDescription.hidden = false;
    lblFrom.hidden = false;
    lblTo.hidden = false;
    eventDateFrom.hidden = false;
    eventDateEnd.hidden = false;
}
@end
